﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MergeSort
{
    class Program
    {
        static void Main(string[] args) {
            var list = new List<int>() { 1, 4, 3, 7, 6, 5, 10, 11 };
            var sorted = MergeSort(list);
            Console.WriteLine($"List before sorting: {list}");
            Console.WriteLine($"List after sorting: {sorted}");
        }

        static List<int> Merge(List<int> left, List<int> right) {
            var sorted = new List<int>();

            // Compares the first values in the to-be merged arrays
            // and adds the larger value to the sorted array. 
            while (left.Any() && right.Any()) {
                if (left[0] < right[0]) {
                    sorted.Add(left[0]);
                    left.RemoveAt(0);
                } else {
                    sorted.Add(right[0]);
                    right.RemoveAt(0);
                }
            }

            // This essentially appends any values left 
            // to the sorted array. There is no more comparisons to be made.
            while (left.Any()) {
                sorted.Add(left[0]);
                left.RemoveAt(0);
            }

            while (right.Any()) {
                sorted.Add(right[0]);
                right.RemoveAt(0);
            }

            return sorted;
        }

        static List<int> MergeSort(List<int> list) {
            var size = list.Count;

            if (size <= 1) {
                return list;
            }
            
            // Splits the initial array in half
            var left = list.GetRange(0, size / 2);
            var right = list.GetRange(size / 2, size - (size / 2));

            // And recursively splits and sorts each subarray
            left = MergeSort(left);
            right = MergeSort(right);

            // Each half is then merged in order. 
            return Merge(left, right);
        }
    }
}
